const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const webpackBase = require('./webpack.base.js');

module.exports = merge(webpackBase, {
  plugins: [
    new webpack.DefinePlugin({
      'process.env.APP_NAME': '"OffOn.io"',
      'process.env.NODE_ENV': '"production"',
      'process.env.API_HOST': '"http://api.cod2.info"',
      'process.env.API_VERSION': '"v1"'
    }),

    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),

    new HtmlWebpackPlugin({
      inject: 'body',
      template: path.resolve(__dirname, '../src/index.html'),
      minify: {
        caseSensitive: true,
        collapseWhitespace: true
      }
    }),
    new WebpackCleanupPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }),
    new FaviconsWebpackPlugin({
      title: 'cod2.info',
      logo: path.resolve(__dirname, '../src/app/assets/images/favicon.png'),
      prefix: 'assets/icons/',
      statsFilename: 'iconstats-[hash].json',
      icons: {
        android: true,              // Create Android homescreen icon.
        appleIcon: true,            // Create Apple touch icons.
        appleStartup: false,        // Create Apple startup images.
        coast: { offset: 25 },      // Create Opera Coast icon with offset 25%.
        favicons: true,             // Create regular favicons.
        firefox: true,              // Create Firefox OS icons.
        windows: true,              // Create Windows 8 tile icons.
        yandex: true                // Create Yandex browser icon.
      }
    })
  ]
});
