import Vue from 'vue';
import 'components/Admin/HeaderContent/headerContent';
import template from './content.html';

export default Vue.component('admin-content', {
  template,

  props: {
    title: String
  },

  data() {
    return {};
  },

  methods: {
  }
});
