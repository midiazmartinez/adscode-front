import Vue from 'vue';
import template from './headerContent.html';

export default Vue.component('header-admin-content', {
  template,

  props: {
    title: String
  },

  data() {
    return {};
  },

  methods: {
  }
});
