export default {
  title: {
    home: 'Home',
    signin: 'Log in',
    signup: 'Signup',
    users: 'Users',
    me: 'Profile',
    settings: 'Settings',
    forgot: 'Forgot',
    notFound: 'Not found',
    content: 'Content',
    dashboard: 'Dashboard',
    search: 'Search',
    campaigns: 'Campaigns',
    createCampaign: 'Create campaign'
  },
  message: {
    titleSignup: 'Create an account OffOn.io',
    successSignup: 'New account created successfully',
    acceptTerms: 'I accept the',
    acceptTermsAnd: 'and',
    acceptConditions: 'You must accept the conditions.',
    titleMyProfile: 'My profile',
    titleSettings: 'Settings',
    titleCampaigns: 'Campaigns',
    titleCampaignsCreate: 'Create campaign',
    titleSignin: 'Sign in to OffOn.io',
    titleContent: 'Content',
    titleDashboard: 'Dashboard',
    titleSearchResult: 'Search result',
    titleUsers: 'Users',
    forgotPassword: 'Forgot your password?',
    needAccount: 'Need an account?',
    signUp: 'Sign up',
    restorePassword: 'Restore it',
    forgotTitle: 'Reset your OffOn.io password',
    forgotSubTitle: 'Send your email or your username and we will resend you a link to reset your password.',
    landingPage: {
      code: {
        title: 'All your additional information!',
        subTitle: 'of your offline ad'
      },
      features: {
        title: 'Advantages for the advertiser',
        item1: 'The best complement for your offline campaigns (realtime tracking, digital ECMO, autosegmentted users)',
        item2: 'Strong increase in your offline campaigns ROI',
        item3: 'Advertising information always updated',
        item4: 'Maximum simplicity and the best UX for the final user',
        item5: 'CPC of already interested customers (permission MK) at a CMP cost',
        moreInfo: 'More information'
      },
      footer: {
        copyright: '2017 © OffOn.io',
        made: 'Made with',
        local: 'in Madrid'
      }
    }
  },
  language: {
    en: 'English',
    es: 'Spanish'
  },
  ui: {
    language: 'Language',
    button: {
      btnStarted: 'Signup',
      btnSignup: 'Create a new account',
      btnGo: 'Go',
      btnLogin: 'Log in',
      btnRestorePassword: 'Send restore email'
    },
    link: {
      linkTerm: 'Terms of Service',
      linkPolicy: 'Privacy Policy'
    },
    navbar: {
      login: 'Log in',
      logout: 'Logout',
      users: 'Users',
      settings: 'Settings',
      profile: 'Profile',
      changeEmail: 'Change email',
      changePassword: 'Change password',
      manageAccount: 'Manage account',
      about: 'About',
      features: 'Features',
      contact: 'Contact',
      dashboard: 'Dashboard',
      manage: 'Manage',
      campaigns: 'Campaigns',
      consult: 'Consult',
      create: 'Create'
    },
    placeholder: {
      name: 'Full Name',
      firstSurname: 'First Surname',
      secondSurname: 'Second Surname',
      username: 'Username',
      email: 'Email',
      password: 'Password',
      passwordConfirmation: 'Confirm Password',
      code: 'ENTER THE CODE',
      login: 'Email (or username)',
      search: 'Search...'
    }
  },
  validator: {
    attributes: {
      password_confirmation: 'Confirm Password'
    },
    messages: {
      after: (field, [target]) => `The ${field} must be after ${target}.`,
      alpha_dash: field => `The ${field} may contain alpha-numeric characters as well as dashes and underscores.`,
      alpha_num: field => `The ${field} may only contain alpha-numeric characters.`,
      alpha_spaces: field => `The ${field} may only contain alphabetic characters as well as spaces.`,
      alpha: field => `The ${field} may only contain alphabetic characters.`,
      before: (field, [target]) => `The ${field} must be before ${target}.`,
      between: (field, [min, max]) => `The ${field} must be between ${min} and ${max}.`,
      confirmed: field => `The ${field} confirmation does not match.`,
      credit_card: field => `The ${field} is invalid.`,
      date_between: (field, [min, max]) => `The ${field} must be between ${min} and ${max}.`,
      date_format: (field, [format]) => `The ${field} must be in the format ${format}.`,
      decimal: (field, [decimals] = ['*']) => `The ${field} must be numeric and may contain ${decimals === '*' ? '' : decimals} decimal points.`,
      digits: (field, [length]) => `The ${field} must be numeric and exactly contain ${length} digits.`,
      dimensions: (field, [width, height]) => `The ${field} must be ${width} pixels by ${height} pixels.`,
      email: field => `The ${field} must be a valid email.`,
      ext: field => `The ${field} must be a valid file.`,
      image: field => `The ${field} must be an image.`,
      in: field => `The ${field} must be a valid value.`,
      ip: field => `The ${field} must be a valid ip address.`,
      max: (field, [length]) => `The ${field} may not be greater than ${length} characters.`,
      max_value: (field, [max]) => `The ${field} must be ${max} or less.`,
      mimes: field => `The ${field} must have a valid file type.`,
      min: (field, [length]) => `The ${field} must be at least ${length} characters.`,
      min_value: (field, [min]) => `The ${field} must be ${min} or more.`,
      not_in: field => `The ${field} must be a valid value.`,
      numeric: field => `The ${field} may only contain numeric characters.`,
      regex: field => `The ${field} format is invalid.`,
      required: field => `The ${field} is required.`,
      size: (field, [size]) => `The ${field} must be less than ${size} KB.`,
      url: field => `The ${field} is not a valid URL.`
    }
  }
};
