export default {
  title: {
    home: 'Inicio',
    signin: 'Iniciar sesión',
    signup: 'Regístrate',
    users: 'Usuarios',
    me: 'Mi perfil',
    settings: 'Ajustes',
    forgot: 'Restablecer contraseña',
    notFound: 'No encontrada',
    content: 'Contenido',
    dashboard: 'Tablero',
    search: 'Buscar',
    campaigns: 'Campañas',
    createCampaign: 'Crear campaña'
  },
  message: {
    titleSignup: 'Crear una cuenta OffOn.io',
    successSignup: 'Nueva cuenta creada con exito',
    acceptTerms: 'Acepto las',
    acceptTermsAnd: 'y la',
    acceptConditions: 'Tiene que aceptar las condiciones.',
    titleMyProfile: 'Mi perfil',
    titleSettings: 'Ajustes',
    titleCampaigns: 'Campañas',
    titleCampaignsCreate: 'Crear campaña',
    titleSignin: 'Iniciar sesión en OffOn.io',
    titleContent: 'Contenido',
    titleDashboard: 'Tablero',
    titleSearchResult: 'Resultado de búsqueda',
    titleUsers: 'Usuarios',
    forgotPassword: '¿Olvidó su contraseña?',
    needAccount: '¿Aún no tiene una cuenta?',
    signUp: 'Cree una ahora',
    restorePassword: 'Restablézcala',
    forgotTitle: 'Restablecer su contraseña de OffOn.io',
    forgotSubTitle: 'Envíe su dirección de correo electrónico o su usuario y le reenviaremos un enlace para restablecer su contraseña.',
    landingPage: {
      code: {
        title: 'Toda la información adicional!',
        subTitle: 'de tu anuncio online'
      },
      features: {
        title: 'Ventajas para el anunciante',
        item1: 'El mejor complemento para tus campañas offline (real time tracking, eco digital, segmentación)',
        item2: 'Incremento radical del ROI de tus campañas offline',
        item3: 'Información publicitaria siempre actualizada para el consumidor',
        item4: 'Máxima sencillez y la mejor experiencia para el cliente final',
        item5: 'CPC de consumidores interesados (permission MK) a precio de CPM',
        moreInfo: 'Más información'
      },
      footer: {
        copyright: '2017 © OffOn.io',
        made: 'Hecho con',
        local: 'en Madrid'
      }
    }
  },
  language: {
    en: 'Inglés',
    es: 'Español'
  },
  ui: {
    language: 'Idioma',
    button: {
      btnStarted: 'Regístrate',
      btnSignup: 'Crear una cuenta nueva',
      btnGo: 'Ir',
      btnLogin: 'Iniciar sesión',
      btnRestorePassword: 'Enviar email de restablecimiento'
    },
    link: {
      linkTerm: 'Condiciones del Servicio',
      linkPolicy: 'Política de Privacidad'
    },
    navbar: {
      login: 'Iniciar sesión',
      logout: 'Cerrar sesión',
      users: 'Usuarios',
      settings: 'Ajustes',
      profile: 'Perfil',
      changeEmail: 'Cambiar email',
      changePassword: 'Cambiar contraseña',
      manageAccount: 'Administrar cuenta',
      about: 'Sobre',
      features: 'Caracteristicas',
      contact: 'Contacto',
      dashboard: 'Tablero',
      manage: 'Administrar',
      campaigns: 'Campañas',
      consult: 'Consultar',
      create: 'Crear'
    },
    placeholder: {
      name: 'Nombre completo',
      firstSurname: 'Primer Apellido',
      secondSurname: 'Segundo Apellido',
      username: 'Nombre de usuario',
      email: 'Correo electrónico',
      password: 'Contraseña',
      passwordConfirmation: 'Confirmar contraseña',
      code: 'INTRODUCE EL CÓDIGO',
      login: 'Email (o nombre de usuario)',
      search: 'Buscar...'
    }
  },
  validator: {
    attributes: {
      password_confirmation: 'Confirmar contraseña'
    },
    messages: {
      after: (field, [target]) => `El campo ${field} debe ser posterior a ${target}.`,
      alpha_dash: field => `El campo ${field} solo debe contener letras, números y guiones.`,
      alpha_num: field => `El campo ${field} solo debe contener letras y números.`,
      alpha: field => `El campo ${field} solo debe contener letras.`,
      before: (field, [target]) => `El campo ${field} debe ser anterior a ${target}.`,
      between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
      confirmed: (field, [confirmedField]) => `El campo ${field} no coincide con ${confirmedField}.`,
      date_between: (field, [min, max]) => `El campo ${field} debe estar entre ${min} y ${max}.`,
      date_format: (field, [format]) => `El campo ${field} debe tener formato formato ${format}.`,
      decimal: (field, [decimals] = ['*']) => `El campo ${field} debe ser númerico y contener ${decimals === '*' ? '' : decimals} puntos decimales.`,
      digits: (field, [length]) => `El campo ${field} debe ser númerico y contener exactamente ${length} dígitos.`,
      dimensions: (field, [width, height]) => `El campo ${field} debe ser de ${width} pixeles por ${height} pixeles.`,
      email: field => `El campo ${field} debe ser un correo electrónico válido.`,
      ext: field => `El campo ${field} debe ser un archivo válido.`,
      image: field => `El campo ${field} debe ser una imagen.`,
      in: field => `El campo ${field} debe ser un valor válido.`,
      ip: field => `El campo ${field} debe ser una dirección ip válida.`,
      max: (field, [length]) => `El campo ${field} no debe ser mayor a ${length} caracteres.`,
      mimes: field => `El campo ${field} debe ser un tipo de archivo válido.`,
      min: (field, [length]) => `El campo ${field} debe tener al menos ${length} caracteres.`,
      not_in: field => `El campo ${field} debe ser un valor válido.`,
      numeric: field => `El campo ${field} debe contener solo caracteres númericos.`,
      regex: field => `El formato del campo ${field} no es válido.`,
      required: field => `El campo ${field} es obligatorio.`,
      size: (field, [size]) => `El campo ${field} debe ser menor a ${size} KB.`,
      url: field => `El campo ${field} no es una URL válida.`
    }
  }
};
