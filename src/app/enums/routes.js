// Public views
import PublicLayout from 'layoutpublic/Layout/layout';
import PublicContentLayout from 'layoutpubliccontent/Layout/layout';
import Home from 'viewspublic/Home/home';
import NotFound from 'viewspublic/NotFound/notFound';
import Signin from 'viewspublic/Signin/signin';
import Signup from 'viewspublic/Signup/signup';
import Forgot from 'viewspublic/Forgot/forgot';
import Content from 'viewspublic/Content/content';

// Administration views
import AdminLayout from 'layoutadmin/Layout/layout';
import Users from 'viewsadmin/Users/users';
import Me from 'viewsadmin/Me/me';
import Settings from 'viewsadmin/Settings/settings';
import Dashboard from 'viewsadmin/Dashboard/dashboard';
import SearchResult from 'viewsadmin/SearchResult/searchResult';
import ConsultCampaign from 'viewsadmin/Campaigns/consult';
import CreateCampaign from 'viewsadmin/Campaigns/create';

export default [
  { path: '/',
    component: PublicLayout,
    children: [
      { path: '/', component: Home, meta: { titleKey: 'home' } },
      { path: 'notfound', component: NotFound, meta: { titleKey: 'notFound' } }
    ]
  },
  { path: '/public',
    component: PublicContentLayout,
    children: [
      { path: '/', component: Signin, meta: { titleKey: 'signin', notauth: true } },
      { path: 'signin', component: Signin, meta: { titleKey: 'signin', notauth: true } },
      { path: 'signup', component: Signup, meta: { titleKey: 'signup', notauth: true } },
      { path: 'forgot', component: Forgot, meta: { titleKey: 'forgot', notauth: true } },
      { path: '*', component: NotFound, meta: { titleKey: 'notFound' } }
    ]
  },
  { path: '/admin',
    component: AdminLayout,
    children: [
      { path: '/', component: Dashboard, meta: { titleKey: 'dashboard', auth: true } },
      { path: 'search/:search', name: 'admin-search', component: SearchResult, meta: { titleKey: 'search', auth: true } },
      { path: 'dashboard', component: Dashboard, meta: { titleKey: 'dashboard', auth: true } },
      { path: 'me', component: Me, meta: { titleKey: 'me', auth: true } },
      { path: 'settings', component: Settings, meta: { titleKey: 'settings', auth: true } },
      { path: 'campaigns', component: ConsultCampaign, meta: { titleKey: 'campaigns', auth: true } },
      { path: 'create-campaign', component: CreateCampaign, meta: { titleKey: 'createCampaign', auth: true } },
      { path: 'users', component: Users, meta: { titleKey: 'users', super: true } },
      { path: '*', component: NotFound, meta: { titleKey: 'notFound' } }
    ]
  },
  { path: '/:code', component: Content, meta: { titleKey: 'content' } }
];
