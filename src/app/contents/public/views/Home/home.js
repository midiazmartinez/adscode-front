import Vue from 'vue';
import _ from 'lodash';
import EnumApp from 'enums/app';
import template from './home.html';
import 'components/ShareButtons/shareButtons';

export default Vue.extend({
  template,

  data() {
    return {
      code: '',
      urlPresentation: ''
    };
  },

  created() {
    this.urlPresentation = EnumApp.URL_PRESENTATIONS_LOCALES[this.$i18n.locale];
  },

  computed: {
    checkCode() {
      return !(_.trim(this.code) !== '');
    }
  },

  methods: {
    goToContent() {
      this.$router.push(`/${this.code}`);
    },

    checkCodeSubmit() {
      if (!this.checkCode) {
        this.goToContent();
      }
    }
  }
});
