import Vue from 'vue';
import template from './signup.html';
import endpoint from 'endpoints';

export default Vue.extend({
  template,

  data() {
    return {
      user: {
        username: '',
        password: '',
        email: '',
        person: {
          name: ''
        }
      },
      conditions: false,
      confirmationPassword: '',
      succes: false,
      error: ''
    };
  },

  created() {
    this.$validator.setLocale(this.$i18n.locale);
  },

  methods: {
    createNewAccount(e) {
      this.$validator.validateAll();
      const hasErrors = this.errors.any();
      if (!hasErrors && this.conditions) {
        this.$http.post(endpoint.SIGNUP, this.user).then((response) => {
          const res = response.body;
          if (res.code === 'CREATED' && res.data) {
            localStorage.setItem('token', res.data.token);
            this.succes = true;
          }
        }, (response) => {
          this.error = response.body.message;
        });
      } else if (hasErrors) {
        e.preventDefault();
      } else {
        this.error = this.$t('message.acceptConditions');
      }
    }
  }
});
