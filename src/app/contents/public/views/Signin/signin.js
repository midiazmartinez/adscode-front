import Vue from 'vue';
import _ from 'lodash';
import template from './signin.html';
import auth from 'services/auth';

export default Vue.extend({
  template,

  data() {
    return {
      credentials: {
        user: '',
        password: ''
      },
      error: ''
    };
  },

  methods: {
    checkCredetials() {
      return !(_.trim(this.credentials.user) !== '' && _.trim(this.credentials.password) !== '');
    },

    submit() {
      if (!this.checkCredetials()) {
        $(this.$refs.btnLogin).button('loading');
        auth.signin(this, {
          user: this.credentials.user,
          password: this.credentials.password
        }, '/admin/dashboard');
      }
    },

    onError(error) {
      $(this.$refs.btnLogin).button('reset');
      this.credentials.user = this.credentials.password = '';
      this.error = error.message;
      this.$refs.user.focus();
    }
  }
});
