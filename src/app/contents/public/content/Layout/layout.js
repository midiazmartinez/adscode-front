import Vue from 'vue';
import template from './layout.html';
import PublicContentHeader from 'layoutpubliccontent/Header/header';

export default Vue.extend({

  template,

  components: {
    PublicContentHeader
  }
});
