import Vue from 'vue';
import template from './layout.html';
import PublicHeader from 'layoutpublic/Header/header';

export default Vue.extend({

  template,

  components: {
    PublicHeader
  }
});
