import Vue from 'vue';
import template from './header.html';
import auth from 'services/auth';
import 'libs/jquery.easing.min.js';
import 'libs/scrolling-nav.js';
import PublicAuthenticated from 'layoutpublic/Authenticated/authenticated';

export default Vue.extend({
  template,

  components: {
    PublicAuthenticated
  },

  data() {
    return {
      user: auth.user
    };
  },

  methods: {
    logout() {
      auth.logout();
    },

    goToSignin() {
      this.$router.push('/public/signin');
    },

    goToSignup() {
      this.$router.push('/public/signup');
    }
  }
});
