import Vue from 'vue';
import template from './aside.html';
import auth from 'services/auth';
import 'metismenu';

export default Vue.extend({
  template,

  mounted() {
    Vue.nextTick(() => {
      $('#side-menu').metisMenu();
    });
  },

  data() {
    return {
      user: auth.user,
      search: ''
    };
  },

  methods: {
    logout() {
      auth.logout();
    },

    onSearch() {
      this.$router.push({ name: 'admin-search', params: { search: this.search } });
      this.search = '';
    }
  }
});
