import Vue from 'vue';
import template from './header.html';
import AdminAside from 'layoutadmin/Aside/aside';
import AdminAuthenticated from 'layoutadmin/Authenticated/authenticated';
import 'libs/sb-admin.js';

export default Vue.extend({

  template,

  components: {
    AdminAside,
    AdminAuthenticated
  }
});
