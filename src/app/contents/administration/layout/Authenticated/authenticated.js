import Vue from 'vue';
import template from './authenticated.html';
import auth from 'services/auth';

export default Vue.extend({

  template,

  data() {
    return {
      user: auth.user,
      email: auth.email
    };
  },

  methods: {
    logout() {
      auth.logout();
    }
  }
});
