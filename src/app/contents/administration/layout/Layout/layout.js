import Vue from 'vue';
import template from './layout.html';
import AdminHeader from 'layoutadmin/Header/header';
import 'components/Admin/Content/content';

export default Vue.extend({

  template,

  components: {
    AdminHeader
  }
});
