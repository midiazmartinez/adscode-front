import Vue from 'vue';
import template from './searchResult.html';

export default Vue.extend({
  template,

  data() {
    return {
      search: this.$route.params.search,
      error: '',
    };
  },

  watch: {
    $route(to) {
      this.search = to.params.search;
    }
  },

  methods: {
  },
});
