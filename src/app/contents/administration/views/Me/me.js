import Vue from 'vue';
import template from './me.html';
import auth from 'services/auth';
import endpoint from 'endpoints';

export default Vue.extend({
  template,

  data() {
    return {
      perfil: null,
      error: '',
    };
  },

  created() {
    this.getMe();
  },

  methods: {
    getMe() {
      this.$http.get(endpoint.ME, {
        headers: auth.getAuthHeader(),
      }).then((response) => {
        const res = response.body;
        if (res.code === 'OK' && res.data) {
          this.perfil = res.data;
        }
      }, (response) => {
        this.error = response.body.message;
      });
    },
  },
});
