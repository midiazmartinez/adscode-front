import router from 'boot/router';
import jwtDecode from 'jwt-decode';
import endpoint from 'endpoints';
import roles from 'services/roles';
import _ from 'lodash';

export default {

  user: {
    authenticated: false,
    roles: null,
    name: '',
    email: ''
  },

  setTokenAndUser(token) {
    const tokenDecode = jwtDecode(token);
    localStorage.setItem('token', token);
    this.user.authenticated = true;
    this.user.name = tokenDecode.name;
    this.user.email = tokenDecode.email;
    this.user.roles = roles.setRoles(_.min(tokenDecode.roles));
  },

  signin(context, creds, redirect) {
    context.$http.post(endpoint.LOGIN, creds).then((response) => {
      const res = response.body;
      if (res.code === 'OK' && res.data) {
        this.setTokenAndUser(res.data.token);
        if (redirect) {
          router.push(redirect);
        }
      }
    }, (response) => {
      context.onError(response.body);
    });
  },

  signup(context, creds, redirect) {
    context.$http.post(endpoint.SIGNUP, creds).then((response) => {
      const res = response.body;
      if (res.code === 'OK' && res.data) {
        this.setTokenAndUser(res.data.token);
        if (redirect) {
          router.push(redirect);
        }
      }
    }, (response) => {
      context.error = response.body.message;
    });
  },

  refresh_token(context, redirect) {
    context.$http.post(endpoint.REFRESH_TOKEN, {
      token: this.getAuthHeader()
    }).then((response) => {
      const res = response.body;
      if (res.code === 'OK' && res.data) {
        this.setTokenAndUser(res.data.token);
        if (redirect) {
          router.push(redirect);
        }
      }
    }, (response) => {
      const err = response.body;
      console.log(err.message);
      this.logout();
    });
  },

  logout() {
    localStorage.removeItem('token');
    this.user.authenticated = false;
    router.push('/');
  },

  checkAuth() {
    const token = localStorage.getItem('token');
    const tokenDecode = token ? jwtDecode(token) : {};
    this.user.authenticated = !!token;
    this.user.name = token ? tokenDecode.name : '';
    this.user.email = token ? tokenDecode.email : '';
    this.user.roles = token ? roles.setRoles(_.min(tokenDecode.roles)) : null;
  },

  getAuthHeader() {
    const token = localStorage.getItem('token');
    return !_.isNull(token) ? {
      Authorization: `JWT ${token}`
    } : {};
  }
};
