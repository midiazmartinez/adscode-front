import Vue from 'vue';
import VueI18n from 'vue-i18n';
import enUS from 'locales/en';
import esES from 'locales/es';

Vue.use(VueI18n);

const locale = localStorage.getItem('lang') || 'en';

const i18n = new VueI18n({
  locale,
  messages: {
    en: enUS,
    es: esES
  }
});

export { i18n as default };
