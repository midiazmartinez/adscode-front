import Vue from 'vue';
import router from './router';
import i18n from './locale';
import './resource';
import './validator';
import './directive';
import 'styles/main.sass';
import auth from 'services/auth';
// Global registry components
import 'components/Language/language';

// Check the user's auth status when the app starts
auth.checkAuth();

export { Vue, router, i18n };
