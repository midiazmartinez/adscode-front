import Vue from 'vue';
import VueValidate from 'vee-validate';
import en from 'locales/en';
import es from 'locales/es';

Vue.use(VueValidate, {
  locale: localStorage.getItem('lang') || 'en',
  dictionary: { // dictionary object
    en: en.validator,
    es: es.validator
  }
});
