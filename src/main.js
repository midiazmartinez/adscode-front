import { Vue, router, i18n } from 'boot/core';

new Vue({ router, i18n }).$mount('#app');
